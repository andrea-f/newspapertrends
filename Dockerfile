FROM ubuntu:14.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
#Runit
RUN apt-get install -y runit 
CMD /usr/sbin/runsvdir-start
# Don't worry about user input
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential python-dev python-pip liblapack-dev libatlas-dev gfortran libfreetype6 libfreetype6-dev libpng12-dev python-lxml libyaml-dev g++ libffi-dev
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
RUN apt-get install -y libmysqlclient-dev
RUN apt-get install libxml2-dev libxslt1-dev
ADD requirements2.txt /code/
RUN pip install -r requirements2.txt
#ADD . /code/