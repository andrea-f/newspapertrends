"""
Django settings for hownewspaperswrite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os,sys

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_DIR)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '3y%bpc2(r!2k)$5frh10hfyp$v&xrz#upqprm)0!(9i09my_gd'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

#CACHES = {
#    'default' : {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        'LOCATION': ['127.0.0.1:11211'],
#        'KEY_PREFIX': 'YOUR_PREFIX',
#    }
#}

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'hownewspaperswrite_app'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'hownewspaperswrite.urls'

WSGI_APPLICATION = 'hownewspaperswrite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
sys.path.append(lib)
news = lib+'/newspapers.db'
#from mongoengine import connect
#connect('news')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'news',                      # Or path to database file if using sqlite3.
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': 'admin',                  # Not used with sqlite3.
        'HOST': 'mysqldb',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    }
}
print news
# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
PROJECT_DIR = '/code/django/hownewspaperswrite/media/'
#STATICFILES_DIRS = (
#    os.path.join(BASE_DIR, "media/static"),
#    '/code/django/hownewspaperswrite/media',
#)
STATICFILES_DIRS = (os.path.join(os.path.dirname(__file__),'static'),)

STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

STATIC_URL = '/static/'

ADMIN_MEDIA_PREFIX = '/hownewspaperswrite/static/admin/'

