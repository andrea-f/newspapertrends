from django.conf.urls import patterns, include, url
from hownewspaperswrite_app.views import home, testate, statistiche_generali, trends, summaries, removeId
from django.contrib.auth import views
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'hownewspaperswrite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    (r'^testate/', testate),
    (r'^statistiche/', statistiche_generali),
    (r'^summaries/getId/(?P<id>[0-9]+)', summaries), 
    (r'^summaries/removeId/(?P<id>[0-9]+)', removeId), 
    (r'^summaries/', summaries),
    (r'^trends/', trends),
    (r'^$/', home),
    (r'', home),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
