import os, sys
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ),'..','hownewspaperswrite'))
sys.path.append(lib)
from django.core.exceptions import ObjectDoesNotExist
from termcolor import colored
from hownewspaperswrite_app import models
Entity = models.Entity
PostItem = models.PostItem
SitePost = models.SitePost
Site = models.Site
Summary = models.Summary
NewspaperSummary = models.NewspaperSummary
Article_Count = models.Article_Count
Word = models.Word
import datetime
from pprint import pprint


class SaveDataInDatabase:
    def __init__(self,):
        """"""

   

    def dropTable(request, table_name = "hownewspaperswrite_app_entity_items" ):
        #hownewspaperswrite_app_entity
        #hownewspaperswrite_app_postitem
        from django.db import connection, transaction
        cursor = connection.cursor()
        try:
            cursor.execute("DROP TABLE "+table_name+";")
        except Exception as e:
            print "Error in dropping app_postitem table %s " % e
        




    def resetItemsFreqDist(self):
        """Resets items freq dist to zero for rebasing."""
        results = PostItem.most_common.delete_everything()
        #results = PostItem.objects.all().delete()
        print "Resetting %s items..." % results.count()
        return
        updated = 0
        for res in results:
            if res.numeric != 0:
                print "Resetting: %s # %s" % (res.word, updated)
                res.numeric = 0
                res.save()
                updated += 1
        return updated

 
    def addSite(self, testate = "", url = "", thumbnail = "", description = "", language = 'eng'):
        """Creates site information."""
        if len(url) == 0:
            raise Exception("No url")
        try:
            r = Site.objects.create(title=testate, url=url, thumbnail = thumbnail, description = description, language = language)
            createdSite = True
        except:
            r = Site.objects.get(url=url)
            createdSite = False
        print "AddSite: Site was created? %s" % createdSite
        return r

    def addSitePost(self, site, testo, url_articolo, links, titolo = "", created = False):
        """Save in database all events contained in a file.

        :param listOfEvents:
            List of event objects to save in DB.

        Return int with total saved videos in database.

        """
        if len(titolo) == 0:
            titolo = url_articolo
        #print titolo
        try:
            v = SitePost.objects.get(url_articolo = url_articolo)
                        
        except:
            try:
                v = SitePost.objects.create(
                    testata = site,
                    url_articolo = url_articolo,
                    testo = testo,
                    titolo = titolo
                )
                created = True
                print "Created: %s | URL: %s" % (created, v.url_articolo)

            except Exception as e:
                print "[AddToModels][addSitePost] Error in creating SitePost object for %s - %s" % (url_articolo,e)

        tot = 0        
        if created is True:
            saved_links = []    
            for link in links:
                if "http" in link and len(link)>3:
                    try:
                        saved_links.append(self.addPostItem(site,link,'LINK', url_articolo))
                    except Exception as e:
                        print "[AddToModels][addSitePost] ERROR in saving item: %s" % e
                        pass
            for link in saved_links:
                v.links.add(link)
                tot+=1
        print "AddSitePost: Url: %s | Created? %s | Links created: %s" % (url_articolo, created, tot)
        return created
        
    

    def addPostItem(self, site, word, tipo, parent_url = "", numeric = 0, tfidf = 0, stem = ""):
        """Add video to db

        :param video: Django video DB object containing title, description, url, thumb url ...

        Return two variables with:
            * **v** -- Created video object in database, dict.
            * **created** -- True if video is created, false if video is already present, bool.
        """
        try:
                v = PostItem.objects.get(word = word, testata_nome = site.title[:250])
                try:
                    print colored("Updating: %s | %s | %s | %s | %s" % (word,v.testata.title, tipo,v.numeric, numeric), "red")
                except Exception as e:
                    print "Error in updating: %s" %e            
                v.numeric = int(v.numeric)+int(numeric)                
        except ObjectDoesNotExist:
                if len(site.title) > 254:
                    tit = site.title[:254]
                elif len(site.title) == 0:
                    print "[addPostItem] site.title has len 0"
                    tit = word
                else:
                    tit = site.title
                try:
                    #print "[addPostItem] site.title is %s" % tit
                    
                    v = PostItem.objects.create(
                        word = word,
                        tipo = tipo,
                        url_articolo = parent_url,
                        numeric = numeric,
                        testata = site,
                        testata_nome = tit
                    )
                    #v.testate.add(site)
                    try:
                        print colored("Saving: %s | %s | %s | %s" % (word,site.title, tipo, numeric), "green")
                    except: 
                        pass
                except Exception as e:

                    print "[addPostItem] ERROR: %s" % e
                    try:
                        print "[addPostItem] %s %s %s %s %s" % (word,tipo,parent_url,numeric,str(site),tit)
                    except:
                        pass                
        if len(stem) > 0:
            v.stem = stem
        if tfidf > 0:
            #db specific hack
            v.tfidf = int(tfidf * 100000)
        try:
            print colored("Final: %s | %s | %s | %s | %s\n" % (word,site.title, tipo,v.numeric, v.stem), "white")
        except:
            pass
        return v

    def addEntity(self, nome= "", word = "", tipo = "", subtipo = "", value = 0, articles_ref = [], categoria = "cd ", add_items = True, add_articles = False):
        print colored("Saving %s from %s..." % (nome, word), "green")
        
        try:
            e = Entity.objects.get(name = nome)
        except ObjectDoesNotExist:
            e = Entity.objects.create(
                name = nome,
                tipo = tipo,
                subtipo = subtipo,
                category = categoria

            )

        if value != e.valore:
            e.valore = value
        if add_items is True:
            posts = PostItem.stems.filter(word__istartswith = word)
            c = 0
            for post in posts:
                c +=1
                e.items.add(post)
            print "Linked %s to %s items." % (nome, c)
        
        

        def saveArticle(e,articles):
            d = 0
            for article in articles:
                d +=1
                #TYPOOOOOOOOOOO
                e.aritcles.add(article)
                print "Linked %s to %s articles." % (nome,d)
                e.save()
                return e

        if add_articles is True:
            articles = SitePost.objects.filter(testo_icontains = word)
            e = saveArticle(e, articles)
        if len(articles_ref) > 0:
            e = saveArticle(e, articles_ref)
        try:
            print colored("Saved %s" % e.name, "yellow")
        except:
            pass
        return e
        

    def addArticlesToEntity(self, entity = {}):
        """Adds articles to entity."""
        #from models import Entity, PostItem, SitePost
        if len(entity) == 0:
            entities = Entity.objects.all()
        else:
            entities = [entity]
        c = 0
        for ent in entities:
            print "Matching articles for %s..." % ent.name
            articles = SitePost.objects.filter(testo__icontains = ent.name)
            for article in articles:
                for ent_art in ent.aritcles.all():
                    if article.titolo not in ent_art.titolo:
                        ent.aritcles.add(article)
                        c += 1
                        ent.save()
            try:
                print "[AddToModels][addArticlesToEntity] Saved %s for %s " % (ent.aritcles.count(), ent.name)
            except Exception as e:
                print "[AddToModels][addArticlesToEntity] Error in printing: %s" % e
        return {
            "entities": len(entities),
            "total_articles": c
        }

    def _updateArticlesCounts(self, summary, testata, numeric):
        article_count = False
        if summary:
            article_count = Article_Count.objects.create(
                testata = testata,
                numeric = numeric
            )
        return article_count
            
    def _saveWord(self, word_str, stats, word_type):
        word = Word.objects.create(
            word = word_str,
            numeric = stats['numeric'],
            percentage = stats['percentage'],
            word_type = word_type
        )

        return word

    def _updateNewspaperSummary(self, summary, newspaper_name, data, word_type):
        newspaper_summary = False
        if summary:
            print "[AddToModels][_updateNewspaperSummary] Updating %s..." % newspaper_name
            newspaper_summary = NewspaperSummary.objects.create(
                name = newspaper_name
            )
            p = 0

            #print "[AddToModels][_updateNewspaperSummary] %s " % data
            #print "update: %s" % data[0]
            for item in data:
                word_str = item[0]
                stats = item[1]
                word = self._saveWord(word_str, stats, word_type)
                try:
                    print "[AddToModels][_updateNewspaperSummary] Saving: %s, %s, %s" % (word.word, newspaper_summary.name, word_type)
                except Exception as e:
                    print "[AddToModels][_updateNewspaperSummary] Error in saving: %s" % e
                    pass
                
                newspaper_summary.words.add(word)
                p += 1
            print "[AddToModels][_updateNewspaperSummary] Saved %s words" % p
        return newspaper_summary

    def addDailySummary(self, data = {}, create_unfiltered = False, create = False):
        """
            data = {
                "total":{
                    "words":[],
                    "all_words":[],
                    "articles":[],
                    "entities":[]
                },
                "date": "today",  
                "newspapers":[{}]

            }
        """
        total_words = data['total_words']
        allWords = data['allwords']
        newspapers = data['newspapers']
        topused = data['topused']
        giornali = data['giornali']
        try:
            # Try to get latest report
            if create is False:
                yesterday = datetime.date.today() - datetime.timedelta(days=1)
                summary = Summary.objects.get(created_at__gt=yesterday)
        except ObjectDoesNotExist:
            create = True

        if create:
            summary = Summary.objects.create(
                words_count = total_words
            )        
        # Number of articles per newspaper
        n = 0
        print "[AddToModels][addDailySummary] Creating articles_count for %s " % summary.created_at 
        for key, value in newspapers.items():
            article_count = self._updateArticlesCounts(summary, testata= key, numeric = value)
            summary.articles_count.add(article_count)
            n += 1
        print "[AddToModels][addDailySummary] Created %s articles_count. " % n

        # u'The Guardian': [('Words', {'numeric': 254744, 'percentage': 100.0}),
        #   (u'World', {'numeric': 2951, 'percentage': 1.158}),
        #   (u'Africa', {'numeric': 1515, 'percentage': 0.595}),
        #   (u'people', {'numeric': 1227, 'percentage': 0.482})]
        
        g = 0
        print "[AddToModels][addDailySummary] Creating newspaper_summary for %s " % summary.created_at 
        #### ISSUE IS HERE
        for newspaper_name, data1 in giornali.items():
            try:
                print "[AddToModels][addDailySummary] Saving daily summary for: %s with %s words." % (newspaper_name,len(data1))
                
            except Exception as e:
                print "[AddToModels][addDailySummary] Error: %s" % e
                pass
            #print data1
            print newspaper_name, data1[0]

            newspaper_summary = self._updateNewspaperSummary(summary, newspaper_name, data1, word_type = "daily_"+str(newspaper_name))
            for res in newspaper_summary.words.all():
                try:
                    print res.word, newspaper_summary.name, res.word_type
                except:
                    pass
            summary.newspapers.add(newspaper_summary)
            g += 1
        print "[AddToModels][addDailySummary] Created %s newspaper_summary. " % g

        t = 0
        if create_unfiltered:
            print "[AddToModels][addDailySummary] Creating words for %s " % summary.created_at 
            for w in allWords:
                
                word = self._saveWord(
                    w[0], 
                    {
                        "numeric": w[1],
                        "percentage": w[2]
                    },
                    word_type = "unfiltered_top"
                )
                #try:
                #    print "[AddToModels][addDailySummary] Saving: %s" % word.word
                #except:
                #    pass
                summary.topused.add(word)
                t += 1
            print "[AddToModels][addDailySummary] Created %s unfiltered top words. " % t

        v = 0
        print "[AddToModels][addDailySummary] Creating top filtered for %s " % summary.created_at 
        for w in topused:
            word = self._saveWord(
                w[0], 
                w[1],
                word_type = "filtered_top"
            )
            summary.topused.add(word)
            v += 1
            print "[AddToModels][addDailySummary] Created %s filtered top words. " % v
        print "[AddToModels][addDailySummary] Created summary at %s  " % summary.created_at

        return summary
