import os,sys
from hownewspaperswrite_app import models
SitePost = models.SitePost
PostItem = models.PostItem
Site = models.Site
Entity = models.Entity
Summary = models.Summary
Word = models.Word


class Delete(object):
	def __init__(self):
		pass

	def deleteById(self, id, table):
		available_tables = {
			"sitepost": SitePost,
			"postitem": PostItem,
			"site": Site,
			"entity": Entity,
			"summary": Summary
		}

		print "[Delete][deleteById] Removing: %s from %s" % (id, str(table))
		try:
			deleted = available_tables[table].objects.filter(id=id).delete()
		except Exception as e:
			print "[Delete][deleteById] Table %s deleting error: %s" % (table, e)
		return deleted

	def deleteAllSummaryWords(self, word_type = None, created_at = None):
		deleted = 0
		if word_type is None and created_at is None:
			deleted = Word.objects.all().delete()
		print "[Delete][deleteAllSummaryWords] Deleted: %s summary words." % len(deleted)
		return len(deleted)

	def deleteSummaries(self):
		Article_Count= models.Article_Count
		Article_Count.objects.all().delete()
		Summary = models.Summary      
		Summary.objects.all().delete()
		Word = models.Word
		Word.objects.all.delete()
		NewspaperSummary = models.NewspaperSummary
		NewspaperSummary.objects.all.delete()