
from __future__ import division
import os, sys
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
sys.path.append(lib)
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext

import operator
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ),'..','..', '..', 'tools'))
sys.path.append(lib)
import Matrix
import TextOperations
import NerExtraction
import pprint
from Read import Read
from Delete import Delete
from AddToModels import SaveDataInDatabase
from pprint import pprint
class GeneralDataView(object):
    """This class formulates the response to the web query."""

    def __init__(self):
        #result = hasattr(DataManagement,req)()
        """"""
        self.read = Read()
        self.delete = Delete()
        self.save = SaveDataInDatabase()

    def trends(self, request):
        """Get trends for politics organizations etc..."""
        filter = "Politici"
        fil_name = "Politic"
        tipo = "PERSON"
        nomi = sorted(self.read.getEntities(tipo = tipo, filter = fil_name).items(), key = lambda x:x[1], reverse = True)
        sys.stderr.write("Fetched: %s" % (str(nomi)))
        sys.stderr.flush()
        
        tutti = self.read.getEntities(tipo = tipo)
        return render_to_response('trends.html', locals(), RequestContext(request))

    def getSummaries(self,request):
        summaries = self.read.getAllSummaries()
        #articles = summary.articles_count.all()
        return render_to_response('base.html', locals(), RequestContext(request))

    def deleteSummaryById(self, request, id):
        deleted = self.delete.deleteById(id, "summary")
        return render_to_response('all_summaries.html', locals(), RequestContext(request))

    def getSummaryById(self, request, id):
        summary = self.read.getSummaryById(id)
        summaries = self.read.getAllSummaries()
        return render_to_response('summary.html', locals(), RequestContext(request))

    def fetch(self, request):
        data = self.getDataFromDBorCache(force_fetch = True)
        newspapers = data["newspapers"]
        total_words = data["total_words"]
        allwords = data["allwords"]
        topused = data["topused"]
        giornali = data["giornali"]

        sys.stderr.flush()
        sys.stderr.write("Fetched: %s words." % (str(total_words)))
        sys.stderr.flush()
        return render_to_response('statistics.html', locals(), RequestContext(request))

    def _getData(self, max_words = 2000):
        """Get all data and statistics from database."""
        # Number of articles per newspaper
        newspapers = self.read.getTotalArticles()

        # Count all words
        total_words = self.read.getTotalWordsInt()
        
        # All top words, unfiltered
        # allw = [(word,occurrences)]
        allw = self.read.getAllTopWords()
        allwords = []
        for w,k in allw.items():
            perc = (float(k/total_words))*100            
            sys.stderr.write(str(w.encode('ascii','ignore')) + " " +str(k) + " "+str(total_words)+" "+str(perc) +" \n")
            allwords.append((w,k, str(perc)[:4]))
        allwords = sorted(allwords, key=lambda tup: tup[1], reverse = True)
        if len(allwords) > max_words:
            allwords = allwords[:max_words]
        # Most used words, filtered
        topused = self.read.getMostCommonWordsTotal()

        print "[GeneralStatistics][_getData] Get most common by site..."
        giornali = self.read.getMostCommonWordsBySite()
        # Most common words per newspaper
        # u'The Guardian': [('Words', {'numeric': 254744, 'percentage': 100.0}),
        #   (u'World', {'numeric': 2951, 'percentage': 1.158}),
        #   (u'Africa', {'numeric': 1515, 'percentage': 0.595}),
        #   (u'people', {'numeric': 1227, 'percentage': 0.482})]
        
        #giornali = self.read.getWordsFreqBySite()
        #giornali = self.read.getTopWordsBySite()
        return {
            "newspapers":newspapers,
            "total_words": total_words,
            "allwords": allwords,
            "topused": topused,
            "giornali": giornali
        }

    def getDataFromDBorCache(self, force_fetch = False):
        """Either save from db or get from cache"""
        if force_fetch is True:
            data = self._getData()
            summary = self.save.addDailySummary(data, create = True)
            return data
        else:
            summary = self.read.getSummary()
            if summary is False:
                self.getDataFromDBorCache(force_fetch = True)
            return summary 

class DataOperations():
    """This class performs nlp analysis on data."""

    def __init__(self, abs_path = ""):
        """"""
        self.abs_path = abs_path
        self.matrix = Matrix.CreateMatrix(abs_path = abs_path)
        self.ner = NerExtraction.NerExtraction()
        self.read = Read()

    def saveMatrix(self, words = {}, abs_path = "", filename = "", min = 0.001, max = 10.0):
        """Generates and saves matrix file from wordlist dictionary of word and occurrences.

        :param words:
            holds "word":445, of all words in database,  dict.
        """
        if len(abs_path) == 0:
            abs_path = self.abs_path
        #testate is dict of newspaper title and associated a dict with words and their frequency
        testate = self.read.getMostCommonWordsBySite(respType = "dict", include_percentage = False)
        print "Generating and saving matrix, calling Matrix()..."
        wordlist = self.matrix.saveMatrix(words, testate, abs_path, filename = filename, min = min, max = max)
        return wordlist

    def hierarchical_clustering(self, abs_path = "", filename = ""):
        """Creates clusters and dendrogram."""
        if len(abs_path) == 0:
            abs_path = self.abs_path
        fn = abs_path+"/"+filename
        print "Definig clusters for %s..." % fn
        dataDict = self.matrix.hierarchical_clustering(filename = fn)
        print "Drawing the dendrogram..."
        imgFile = self.matrix.draw_dendrogram(dataDict['clusters'], dataDict['titles'], jpeg = filename+".jpg")
        return imgFile



    

