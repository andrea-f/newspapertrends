# To change this template, choose Tools | Templates
# and open the template in the editor.

#code provided by www.dinx.tv code is under bsd license
import unittest
import os, sys
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'src'))
sys.path.append(lib)
# '/home/andrea/jobs/newspapertrends/scraper/newspapertrends/bin/src'
import django

lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ),'..','..', 'django', 'hownewspaperswrite'))
sys.path.append(lib)

os.environ['DJANGO_SETTINGS_MODULE'] = 'hownewspaperswrite.settings'
django.setup()
from Fetch import FetchSite as fetch
from Fetch import Fetch as fetchdata
from Fetch import TextAction as ta
from Fetch import ManipulateResults as sr
from pprint import pprint

class  TestURLRetrivalTestCase(unittest.TestCase):
    def setUp(self):
        try:
            self.url = sys.argv[1]
        except:
            self.url = "http://www.theguardian.co.uk"
        try:
            self.language = sys.argv[2]
            
        except:
            self.language = "ita"

        try:
            self.testata = sys.argv[3]
        except:
            self.testata = self.url

        self.abs_path = os.path.abspath(os.path.join(os.path.dirname( __file__ )))

    #

    def tearDown(self):
        """"""
    #    self.foo = None

    def testURLRetrival(self):
        #assert x != y;
        #self.assertEqual(x, y, "Msg");
        f = fetch(self.url,name = self.testata, depth = 3)
        #iterations = f.get("getWordCount")
        iterations = f.get(maxArticles = 25)
        isinstance(iterations, list)
        isinstance(iterations[0], dict)
        t = ta()
        text = ""
        for iteration in iterations:
            try:
                text +=" " + iteration['text']
            except:
                pass
        freqDist = t.getWordCount(text)
        top = {}
        for key, value in freqDist.items():
            if value > 4 and len(key)> 3 and not key.isupper() and (len(key)>5 or not key.islower()) and key.isalpha():
                top[key] = value
        #totalErrors = [int(len(iteration['data'])) for iteration in iterations]
        #errors = [e for iteration in iterations for e in iteration['data']]
        # top
        t = sorted(top.items(), key=lambda x:x[1])
        pprint(t)
        #print "Total pages analysed: %s \nTotal spelling errors: %s\nErrors: %s \%" % (len(iterations), sum(totalErrors), str(errors)[:200])
        #self.fail("TODO: Write test")

    def testSaveInDB(self):

        s = sr()
        #site = s.saveSite(self.testata, self.url, language = self.language)
        f = fetch(self.url, name = self.testata,depth = 1)

        #iterations = f.get("getWordCount")
        iterations = f.get(maxArticles = 7)
        assert isinstance(iterations, list)
        assert isinstance(iterations[0], dict)
        t = ta()
        text = ""
        #each iteration is an article
        allUrls = []
        for iteration in iterations:
            if iteration is not None:
                if iteration['url'] not in allUrls:
                    try:
                        text +=" " + iteration['text']
                    except:
                        pass
                   #sitePost = s.saveSitePost(site, iteration['text'], iteration['url'], links = iteration['links'], titolo = "")
                    allUrls.append(iteration['url'])
        text = ' '.join([w for w in text.split() if len(w)> 4 and not w.isupper() and (len(w)>5 or not w.islower()) and w.isalpha()])
        freqDist = t.getWordCount(text)
        top = {}
        for key, value in freqDist.items():
            key = key.replace(',', '').replace('.', '').replace(':', '')
            if value > 4 and len(key)> 3 and not key.isupper() and (len(key)>5 or not key.islower()) and key.isalpha():
                top[key] = value
                
        #totalErrors = [int(len(iteration['data'])) for iteration in iterations]
        #errors = [e for iteration in iterations for e in iteration['data']]
        # top
        t = sorted(top.items(), key=lambda x:x[1])
        for item in t:
            tipo = "MOSTCOMMONWORD"
            word = item[0]
            value = item[1]
            site = iterations[0]['site']
            postItem = s.savePostItem(site, word, tipo, parent_url = site.url, numeric = value)
        getresults = sr(abs_path = self.abs_path)
        getresults.calculateTfIdf(wordsinput = text, wordmin = 1000000)
        getresults.nerExtraction()

    def testGetItem(self):
        """Return list of items by word."""
        word = "Leggo"
        getresults = sr()
        items = getresults.getItems(testata = word)
        for item in items:
            print item.word, item.testata.title, item.stem, item.tfidf
        

    def testSavePostItems(self):
        """Excutes saves in items in db."""
        tipo = "MOSTCOMMONWORD"
        min = 0.001
        s = sr()
        getresults = sr(abs_path=os.path.abspath(os.path.join(os.path.dirname( __file__ ))), filename=str(min)+"_newspaper" )
        totale_words = getresults.getTopWordsBySite()
        #print "Processing...%s articoli" % len(totale_articoli)
        savedItems = 0
        print "Getting ready to save articoli..."
        for key, value in totale_words.items():
            site = getresults.getSite(title = key)
            for word, value2 in value.items():
                if value2 > 4:
                    savedItems +=1
                    word = word.replace('.', '').replace(',', '').replace(':', '')
                    postItem = s.savePostItem(site,word , tipo, parent_url = site.url, numeric = value2)
        print "Saved %s items." % savedItems
        return postItem


    def testResetItemsFreqDist(self):
        """Resets freq dist of mcws."""
        delete = sr()
        delete.resetItemsFreqDist(tb = 'hownewspaperswrite_app_entity')
        delete.resetItemsFreqDist(tb = 'hownewspaperswrite_app_entity_items')
        #delete.dropTable()

    def testWordAnalysis(self):
        """Prepares data and executes nlp analysis."""
        min = 0.0001
        getresults = sr(abs_path=os.path.abspath(os.path.join(os.path.dirname( __file__ ))), filename=str(min)+"_newspaper" )
        words = getresults.getAllWordsFrequency()    
        assert isinstance(words, dict)
        #wordlist = getresults.saveMatrix(words,os.path.abspath(os.path.join(os.path.dirname( __file__ ))))
        print "Creating and saving  matrix...."
        wordlist = getresults.saveMatrix(words,os.path.abspath(os.path.join(os.path.dirname( __file__ ))), min = min, filename=str(min)+"_newspaper")

        #wordlist = getresults.saveMatrix(giornali,os.path.abspath(os.path.join(os.path.dirname( __file__ ))), min = min, filename=str(min)+"_byarticle_newspaper")

        assert isinstance(wordlist, list)
        img = getresults.hierarchical()
        print "Finished drawing the dendrogram and saved it to: %s" % img
        #pprint(wordlist)

    def testCalculateTfIdf(self):
        """Calculates tfidf for words and saves result in db."""
        getresults = sr(abs_path = self.abs_path)
        c = getresults.calculateTfIdf()
        print "Saved in total %s stems" % len(c)
        #assert isinstance(words, dict)


    def testCalculateTestateTfIdf(self):
        """Calculates tfidf for words and saves result in db."""
        getresults = sr(abs_path = self.abs_path)
        c = getresults.calculateTfIdf()
        print "Saved in total %s stems" % len(c)

    def testNerExtraction(self):
        getresults = sr(abs_path = self.abs_path)
        getresults.nerExtraction(reset = True)

    def testGrammarCheck(self):
        with open ("article.txt", "r") as myfile:
            data=myfile.read().replace('\n', '')
            f = ta()
            txt = f.getText(text = data)
            res = f.checkGrammar(txt)
            assert isinstance(res,list)
            print res

    def testMostCommonWords(self):
        with open ("article.txt", "r") as myfile:
            data=myfile.read().replace('\n', '')
            f = ta()
            txt = f.getText(text = data)
            res = f.mostCommonWords(txt)
            assert isinstance(res,list)
            print res


    def testGetWordCount(self):
        with open ("article.txt", "r") as myfile:
            data=myfile.read().replace('\n', '')
            f = ta()
            txt = f.getText(text = data)
            res = f.getWordCount(txt)
            print res
            assert isinstance(res,dict)
            
    def addArticlesToEntity(self):
        """Adds articles to entities."""
        getresults = sr(abs_path = self.abs_path)
        c = getresults.addArticlesToEntity()
        pprint(c)


if __name__ == '__main__':

    suite = unittest.TestSuite()
    #suite.addTest(TestURLRetrivalTestCase('testGetWordCount'))
    
    #suite.addTest(TestURLRetrivalTestCase('testMostCommonWords'))
    
    #suite.addTest(TestURLRetrivalTestCase('testURLRetrival'))
    
    # 1. Populate data in database with process_news.sh
    try:
        if "add" in sys.argv[4]:
            suite.addTest(TestURLRetrivalTestCase('testSaveInDB'))
    except:
        pass
    # 2. Saves most common words from all words in db, OTHERWISE list of words per newspaper will be empty.
    try:
        if "words" in sys.argv[1]:
            suite.addTest(TestURLRetrivalTestCase('testSavePostItems'))
    except:
        pass
    #suite.addTest(TestURLRetrivalTestCase('testResetItemsFreqDist'))
    # 3. Calculates tfidf for all words in database if test folder is empty of csv files with tfidf information
    #suite.addTest(TestURLRetrivalTestCase('testCalculateTfIdf'))
   

    # 4. Populate calais ner data from most common words
    #suite.addTest(TestURLRetrivalTestCase('testNerExtraction'))
    
    
    #suite.addTest(TestURLRetrivalTestCase('testGetItem'))
    
    #suite.addTest(TestURLRetrivalTestCase('addArticlesToEntity'))


    #suite.addTest(TestURLRetrivalTestCase('testWordAnalysis'))
    
    #suite.addTest(TestURLRetrivalTestCase('testGrammarCheck'))
    unittest.TextTestRunner().run(suite)

