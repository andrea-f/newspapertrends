# To change this template, choose Tools | Templates
# and open the template in the editor.

#Code provided by www.dinx.tv code is under bsd license
__author__="root"
__date__ ="$28-Aug-2014 23:13:12$"
import os, sys
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..'))
sys.path.append(lib)
from tools.WebData import WebData
from tools.TextOperations import TextOperations
from tools.NerExtraction import NerExtraction
from tools.NLTK import NLTK
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..', 'django','hownewspaperswrite', 'hownewspaperswrite_app'))
sys.path.append(lib)
import AddToModels
import GeneralStatistics
from termcolor import colored

import Read
class URLMetadata(object):

    def __init__(self, url ="", links = [], text = "", language = "eng", parent = None, data = [], cleanText = "", *args, **kwargs):
        self.url = url
        self.links = links
        self.text = text
        self.parent = parent
        self.language = language
        self.data = data
        self.cleanText = cleanText

class Fetch(URLMetadata):
    """This class handles fetching an URL from the web."""
    POSSIBLECLASSNAMES = ["chapter-paragraph", "articleBody", "entry-content full-content", 'testo-articolo','content']

    def __init__(self):
        """"""
        self.newspaper = False
        
    def getUrl(self, url = ""):
        """Retrieves URL using `tools.HTTPUtils`

        :param url:
            Resource to fetch, string.

        Return String with raw response text.
        """
        
        if len(str(url)) == 0:
            url = self.url
        else:
            if len(url) == 0:
                raise Exception('URL length cannot be zero.')
                return
            self.url = url        
        httpgetter = WebData()
        result = httpgetter.fetchUrl(url)
        if "rss" in url.lower() or "feed" in url.lower():
            self.newspaper = True
        else:
            self.newspaper = False
        #print "result: %s"%result
        self.text = result
        
        return result

    def getLinks(self, url = "", text = ""):
        """Retrieves links from url. Fetches URL if not loaded, else takes text in currently loaded object"""
        if len(str(text)) == 0:
            text = self.getUrl(url)
        
        httpgetter = WebData()
        links = httpgetter.getLinksFromPage(text)
        #links = [link.replace("<![CDATA[",'').replace("]]>",'') for link in links if link is not None]
        links2 = []
        for link in links:
            try:
                if link is not None:
                    link.replace("<![CDATA[",'').replace("]]>",'')
                    links2.append(link)
            except:
                pass


        
        return links2

    def getText(self, text="", content = ""):
        """Retrieves the text part of an URL"""
        self.cleanText = ""
        if len(text) == 0:
            text = self.text
        httpgetter = WebData()
        for name in self.POSSIBLECLASSNAMES:
            content += httpgetter.getSpecificClassContent(text, htmlClass = name)
        
        if self.newspaper is False:
            if content is None:
                txt = httpgetter.getTextFromPage(text)
        else:
            txt = content
            if len(content) == 0:
                txt = text
        try:
            self.cleanText = txt
        except:
            txt = httpgetter.getTextFromPage(text)
        return txt

    def getSentences(self):
        """Returns text split in sentences"""



class FetchSite():
    """This class fetches a site. Fetching consists of iterating over a page links and getting the text from them."""
    def __init__(self, url, root = "", name = "",lang = "eng", depth = 2):
        """"""
        import urlparse
        self.depth = depth
        self.url = url
        if len(name) == 0:
            rep = ["http://","www."]
            for r in rep:
                name = url.replace(r, '')
        self.name = name
        self.lang = lang
        if len(root) == 0:
            root = url
        self.root = urlparse.urlparse(root).hostname
        if not self.root.endswith('/'):
            self.root +="/"



    def get(self, action = "", url = "", depth = 2, maxArticles = 15, *args, **kwargs):
        iterations = []
        currentDepth = 0
        if len(url) == 0:
            url = self.url
        
        processedLinks = []
        total = 0
        while currentDepth <= depth:
            #take snapshot of iteration

            if currentDepth == 0:                
                iterationData = self.procedure(currentDepth, url = url,action = action, *args, **kwargs)
                iterations.append(iterationData)
                print "Root URL: %s" % url
                processedLinks.append(url)
                total+=1
            else:
                currentLinks = iterationData['links']
                for link in currentLinks:
                    #print "Link #: %s" % len(processedLinks)
                    if link not in processedLinks:
                        url = link
                        try:
                            iterationData = self.procedure(currentDepth, action = action,url = url,*args, **kwargs)
                        except:
                            if "http" not in link:

                                url = self.root+link
                            iterationData = self.procedure(currentDepth, action = action,url = url, *args, **kwargs)
                        #print "self.url: " + self.url
                        iterations.append(iterationData)
                        processedLinks.append(link)
                        total+=1
                    if total == maxArticles:
                        break;
            currentDepth +=1
            total = 0
        return iterations

    def getPageData(self,url):
        """Retrieves url, extracts links, text and returns them."""
        operation = Fetch()
        try:
            page = operation.getUrl(url)
            #print "PAGE: %s" % page
        
        except Exception as e:
            print "%s : ERROR: %s" % (url,e)
            return None
        print "Getting links..."
        links = operation.getLinks(text = page)
    
        print "Found %s links." % len(links)
        print "Removing HTML tags..."
        txt = operation.getText(text = page)
        print "Article URL: %s" % url
        print "LEN TEXT: %s" % len(txt)
        return {
            "links": links,
            "page": page,
            "txt": txt
        }


    def procedure(self, depth, url = "", action = "", *args, **kwargs):
        "Actions to carry out on a page."
        
        mr = ManipulateResults()
        print "\nProcessing URL: %s..." % url
        #print "Page downloaded!"
        article = mr.getPost(url)
        print "article: %s " % article
        if article is False:
            pageData = self.getPageData(url)
            if pageData is None:
                return
            links = pageData['links']
            #page = pageData['page']
            txt = pageData['txt']
            data = []
            if len(action)>0:
                try:
                    data = getattr(TextAction(), action)(*args, **kwargs)
                except:
                    pass

            ####SAVE

            textoperations = TextOperations()
            site = mr.saveSite(url = self.root,testata = self.name, language = self.lang)

            saved = mr.saveSitePost(site, txt, url, links = links, titolo = "")
            saved = False
            #print "ARTICLE TEXT: %s" % self.cleanText
            if saved is True:
                wordcount = textoperations.getWordCount(txt)
                savedItems = 0
                for word,value in wordcount.items():
                    tipo = "MOSTCOMMONWORD"
                    #if value > 4 and len(word)> 3 and not word.isupper() and (len(word)>5 or not word.islower()) and word.isalpha():
                    savedItems +=1
                    postItem = mr.savePostItem(site,word, tipo, parent_url = url, numeric = value)
                ### ENDSAVE
                print "Saved %s ITEMS for article %s" % (savedItems, url)
        else:
            site = article.testata
            #txt = article.testo
            #links = article.links.all()
            txt = ""
            links = [link.word for link in article.links.all()]
            if len(links) == 0:
                pageData = self.getPageData(url)
                links = pageData['links']
                txt = pageData['txt']
            print "Article EXISTS!\nURL: %s\nCreated At: %s\nLinks: %s" % (article.url_articolo,article.created_at, links)

            data = {}
        return {
                "depth": depth,
                "data": data,
                "links": links,
                "text": txt,
                "url": url,
                "site": site
        }

class TextAction(Fetch):

    def checkGrammar(self, text = "", language = "eng"):
        """Checks for grammatical mistakes the input text."""
        print "\nChecking grammar..."

        if len(text) == 0:
            text = self.cleanText
        to = TextOperations()
        errors = to.spellCheck(text.encode('utf8'),language)
        print "Errors found: %s\n" % (errors)

        return errors

    def mostCommonWords(self, text = "", minFreq = 0.1, maxFreq = 0.5):
        print "Getting most common words..."
        to = TextOperations()
        wordsClustered = to.mostCommonWordsInPage("",minFreq, maxFreq, text)
        return wordsClustered

    def getWordCount(self, text):
        """Returns word count with top words first"""
        print "Getting frequency distribution..."
        to = TextOperations()
        topWords = to.getWordCount(text)
        return topWords

if __name__ == "__main__":
    print "Hello World"

class ManipulateResults():
    """Saves result using Django"""

    def __init__(self, testata = "", abs_path = "", filename = "newspapers", language = "eng"):
        """"""
        self.model = AddToModels.SaveDataInDatabase()
        self.getter = Read.Read()
        self.operation = GeneralStatistics.DataOperations(abs_path = abs_path)
        self.ner = NerExtraction()
        self.abs_path = abs_path
        self.filename = filename
        self.testata = testata
        self.language = language

    def resetItemsFreqDist(self, tb = ""):
        """Resets most common words count to zero."""
        updated = self.model.dropTable(table_name = tb)#resetItemsFreqDist()
        print "Updated %s items." % updated

    #def saveProcessed(self, *args, **kwargs):
     #   """Saves site, post and item."""
      #  site = self.saveSite(*args, **kwargs)

    def getReadyToSave(self, totale_articoli):
        """Returns tuple with site instance and text."""
        return self.getter.getReadyToSave(totale_articoli)

    def saveSite(self, url, testata="", language = "", *args, **kwargs):
        if len(testata) == 0:
            testata = self.testata
        if len(language) == 0:
            language = self.language
        site = self.model.addSite(testata, url, language = language, **kwargs)
        return site

    def saveSitePost(self, site, testo, url_articolo, links = [], titolo = ""):
        """"""
        created = self.model.addSitePost(site, testo, url_articolo, links, titolo)
        return created

    def savePostItem(self, site, word, tipo, parent_url = "", numeric = 0 ):
        postItem = self.model.addPostItem(site, word, tipo, parent_url= parent_url, numeric = numeric)
        return postItem

    def getSite(self, title = ""):
        """Retrieves site from db."""
        site = self.getter.getSite(title = title)
        return site

    def getPost(self, url):
        """Gets post from db.
        Either returns an item object or False.
        """
        site = self.getter.getPost(url)
        return site
    
    def getAllWords(self, testata = ""):
        words = self.getter.getAllWords(testata = testata)
        return words

    def getItem(self):
        """Gets item from db.
        Either returns an item object or False."""
        site = self.getter.getItem()
        return site

    def getAll(self, datatype ="item"):
        if "item" in datatype:
            resultSet = self.getter.getMostCommonWords(respType="dict")
        else:
            resultSet = self.getter.getMostCommonWords(respType="dict")
        return resultSet

    def getAllWordsFrequency(self, filter = ""):
        """List of tuples with word and root."""
        totalWords = self.getter.calculateWordFrequencies()
        return totalWords

    def getTopWordsBySite(self, testata = ""):
        """Returns top words by site. Dict with key is site name and value is dict with frequency distribution."""
        data = self.getter.getTopWordsBySite(testata)
        return data

    def saveMatrix(self, words, abs_path = "", min = 0.001, max = 10.0, filename = ""):
        """Creates and saves matrix on disk from wordset.

        :param words:
            Holds string with number of occurrences, dict.
        :param total:
            Total number of words in db, int.

        Return list with tuple of word and presence in text.
        """
        if len(abs_path) == 0:
            abs_path = self.abs_path
        if len(filename) == 0:
            filename = self.filename
        wordlist = self.operation.saveMatrix(words, abs_path, filename = filename, min = min, max = max)
        return wordlist

    def hierarchical(self, abs_path = "", filename = ""):
        """Generates clusters and saves dendrogram."""
        if len(abs_path) == 0:
            abs_path = self.abs_path
        if len(filename) == 0:
            filename = self.filename
        img = self.operation.hierarchical_clustering(abs_path = abs_path, filename = filename)
        return img

    

    def calculateTfIdf(self, wordmin = 500000, wordmax = 2400000, wordsinput = "", testata = ""):
        """Calculaates tfidf for words in database, updates items to reflect new calc, saves stem in item."""
        
        nltkop = NLTK(output_dir = self.abs_path)
        if len(testata) == 0:
            words = nltkop.getTfIdfFile()
            print "[calculateTfIdf] TOTAL WORDS TO TFIDF: %s" % len(words)
        if len(words) == 0:
            
            # words is a massive string of text
            if len(testata) == 0:
                words = self.getter.getAllWords()
            else:
                words = self.getter.getAllWords(testata = testata)
            print "Calculating tfidf for all %s words...for %s" % (len(words), testata)
            w1 = words[:wordmin]
            if len(wordsinput) == 0:
                w2 = words[wordmax:]
            else:
                w2 = wordsinput
            if len(w2) == 0:
                w2 = words[wordmin:]
            tfs = nltkop.calculateTfIdf(w1)
            print "[Fetch][calculateTfIdf] Words to be input to analyseText: %s" % len(w2)
            words = nltkop.analyseText(w2, tfs)
            #words = (stem, tfids)
        words_tl = sorted(words.items(), key=lambda x:x[1], reverse=True)
        c = 0
        w = 0
        print "[Fetch.calculateTfIdf] Matching tfidf words with most common ones..."
        allWords = {}
        for word in words_tl:
            w += 1
            stem = word[0]
            tfidf = word[1]
            items = self.getter.getWord(stem = stem)
            if len(items)>0:
                for item in items:
                    if item.stem is None or len(item.stem) == 0:
                        item.stem = stem
                        item.tfidf =tfidf
                        item.save()
                        try:
                            print "Saved %s for word %s %s completed" % (item.stem, item.word.encode('utf8'), str((float(w)/float(len(words_tl)))*100))
                        except:
                            pass
                        c += 1
            allWords[word] = tfidf
        return allWords

    def getItems(self,word = "", testata = ""):
        items = self.getter.getItems(word, testata = testata)
        return items
        
    def nerExtraction(self, topused = {}, reset = False):
        """Extracts Named Entities from text."""
        if reset:
            print "[Fetch][nerExtraction] Resetting items distribution..."
            try:
                total_reset_items = self.model.resetItemsFreqDist()
                print "[Fetch][nerExtraction] Reset: %s items." % total_reset_items
            except Exception as e:
                print "[Fetch][NerExtraction] Error in resetting items: %s" % e
        if len(topused) == 0:
            topused = self.getter.getMostCommonWordsTotal(include_language = True, respType = 'dict')
        tot = []
        
        c = 0
        for word, data in topused.items():
            articles = []
            c+=1
            if word[0].isupper():
                try:
                    print "Searching for %s" % word
                except:
                    pass
                try:
                    #print "Searching %s in %s" % (word, data['language'])
                    entities = self.getter.getEntity(name = word)
                    if len(entities) == 0:
                        result = self.ner.parse(word, language = data['language'][:2], filter = "person")
                    else:
                        print "Entity: %s exists..." % word
                        result['freebase'] = []
                except:
                    try:
                        print word.encode('utf8')+",", data['language']+"\n"
                    except:
                        pass
                    result = {}
                    result['freebase'] = []
                tipo = "PERSON"
                completed = ((float(c) / float(len(topused))) * 100)
                if len(result['freebase'])>0:

                    for item in result['freebase'][:5]:
                        print item
                        name = item["name"]

                        try:
                            ner = item['notable']['name']
                            cat = item['notable']['id']
                        except:
                            ner = item['name']
                            cat = "Not Available"
                        try:
                            print name.encode('utf8'), word.encode('utf8'), ner, cat
                        except Exception as e:
                            print "[nerExtraction] ERROR: %s" % e

                        entity_info = {
                            "name": name,
                            "ner": ner,
                            "cat": cat,
                            "numeric": data['numeric']
                        }
                        print colored(("Finding all articles for %s..." % name), "white")
                        if word.encode('utf8').lower() in name.encode('utf8').lower():
                            matches = self.getter.getArticlesByWord(name)

                        try:
                            if len(matches) > 0:
                                color = "green"
                            else:
                                color = "red"
                            print colored(("Found %s articles for %s  - %s" % (len(matches),name, completed)), color)

                            if len(matches) > 0:
                                articles.append((entity_info,matches))
                                break
                        except:
                            pass
            #word loop
                if not word in tot:
                    if len(articles)>0:
                        sorted(articles, key=lambda x:len(x[1]))
                        article = articles[0]
                        name = article[0]["name"]
                        matches = article[1]
                        ner = article[0]['ner']
                        cat = article[0]['cat']
                        valore = article[0]['numeric']
                        v = self.model.addEntity(nome= name, value = valore, word = word, tipo = tipo, articles_ref = matches, subtipo = ner, categoria = "TRENDING", add_items = False, add_articles = False)

                        tot.append(word)
        print "Saved %s entities." % len(tot)


        #save result in db

        #pprint.pprint(result['freebase'][0])
        #LOCATION
        #ORGANIZATION
        #POLITICIAN

    def addArticlesToEntity(self, entity = {}):
        saved_info = self.model.addArticlesToEntity(entity = entity)
        return saved_info

    def getWordsNewspapers(self):
        topused = self.getter.getMostCommonWordsTotal(include_language = True, respType = 'dict', )