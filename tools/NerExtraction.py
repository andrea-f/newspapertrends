import os,sys
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), 'api'))
sys.path.append(lib)
from Freebase import Freebase
class NerExtraction(object):
    """This class extracts Name Entities from text."""

    def __init__(self, text = ""):
        """Set main text to parse."""
        self.text = text
        self.tokenized = self._tokenize(text)

    def _tokenize(self, text):
        """Split text in list of words."""
        return text.split()

    def parse(self, text = "", language = "en", filter = ""):
        """Main function of `NerExtraction`, parses a set of text and returns a dict answer."""
        if len(text) == 0 and len(self.text) != 0:
            text = self.text

        freebase_result = self.freebase(text = text, language = language, filter = filter)
        return {
            "freebase":freebase_result
        }

    def freebase(self, text = "", language = "en", filter = ""):
        """Extracts information using freebase db."""
        
        #print "Extracting entities from %s words using Freebase db..." % len(text.split())
        if len(text) == 0:
            text = self.text
        freebase = Freebase()
        result = freebase.search(query = text, language = language, filter = filter)
        return result





