#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib2
import os, sys, traceback
os.environ['http_proxy']=''
from BeautifulSoup import BeautifulSoup as soup
import HTTPUtils
import cookielib

class WebData():
	"""Fetches text data from the web."""
	
	def __init__(self, mainUrl = ""):
		"""Initializes parameters for fetching data."""
		self.mainUrl = mainUrl
		
	def getTag(self, html,tag = "title"):
		"""Gets specific tag from HTML document."""
		import Sanitizer
		return Sanitizer.getTag(html,tag)

	def fetchUrl(self, url = ""):
		"""Fetches data from url."""	
		try:
			response = urllib2.urlopen(url)
		except:
			exc_type, exc_value, exc_traceback = sys.exc_info()
		 	error = "[FetchWebData.fetchUrl] Error whilst running:\n\n%s" % traceback.format_exc(limit=10)
		 	print error		
			#if http_code == 302:
			#This is to handle websites which require cookies to be set in order to load the HTML
			cj = cookielib.CookieJar()
			opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
			request = urllib2.Request(url)
			response = opener.open(request)
		html = response.read()#.decode('utf-8')
		return html
		

	def getTldUrl(self, url):
		"""Retrieves top level domain suffix from url."""
		from tld import get_tld
		res = get_tld(url, as_object=True)
		tld = res.suffix
		#res.subdomain
		# 'some.subdomain'
		#res.domain
		# 'google'
		#res.tld
		return tld

	def getDomain(self,url):
		"""Returns domain name from a URL."""
		from urlparse import urlparse
		parsed_uri = urlparse( url )
		domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
		return domain
		
	def getLinksFromPage(self, page = "<body><a href='123'>qwe</a><a href='456'>asd</a></body>", links = []):
		"""Fetches specific links from html page."""
		outlist = []
		
		try:
				html = soup(page)
		except:
			html = soup(page.encode('utf8'))	   
		#a href
		links = [tag.attrMap['href'] for tag in html.findAll('a', {'href': True})]
		#link rss feed
		links += [tag.next for tag in html.findAll('link')]
		links += [tag.next for tag in html.findAll('guid')]
		for link in links:
			if link not in outlist:
				outlist.append(link)
		return outlist
		
	def getTextFromPage(self, value, VALID_TAGS = ['div','p','span']):
		"""Fetches data from folder"""
		import Sanitizer		
		return Sanitizer.plaintext(value)

	def getFeedRSS(self,url):
		"""Retrieves a RSS feed from a url."""
		import feedparser
		d = feedparser.parse(url)
		return d

	def getSpecificClassContent(self,text, htmlClass="container-body-article clearfix"):
		"""Retrieves html inside a specific class tag."""
		import Sanitizer
		try:
			return Sanitizer.getClassContent(text, htmlClass)
		except:
			try:
				return Sanitizer.getClassContent(text.encode("utf-8"), htmlClass)
			except:
				print "[WebData][getSpecificClassContent] Failing silently..."
				return Sanitizer.getClassContent("", htmlClass)