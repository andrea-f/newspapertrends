import nltk
import string
import FileHandler
import pprint
import datetime

from Counter import Counter
import string
import os

from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer

path = '/opt/datacourse/data/parts'

stemmer = PorterStemmer()



class NLTK(object):
	"""This class performs NLTK operations.

	Code from http://www.cs.duke.edu/courses/spring14/compsci290/assignments/lab02.html
	"""

	def __init__(self, output_dir = ""):
		""""""
		self.tfidf = {}
		lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ),'libraries'))
		self.output_dir = output_dir
		self.tfidf_name = "tfidf_"+self.getDate()+".csv"
		fn = "stopwords_italian.txt"
		self.fileop = FileHandler.FileHandler()
		self.lines = self.fileop.readFileByLine(fileName = lib+"/"+fn)


	def getDate(self, separator = "-"):
		today = datetime.date.today()
		dt = str(today.day)+separator+str(today.month)+separator+str(today.year)
		return dt

	def getData(self, filename = "", source = "db"):
		"""Reads data from file or db."""
		fileop = FileHandler.fileHandler()
		sources = {
			"db": self.readFromDb(),
			"web":"",
			"file":fileop.readFile(filename)
		}
		for s,op in sources.items():
			if source in s:
				text = source[s]
			
		return text

	def readFromDb(self, option = ""):
		"""Read information from database."""
		
	def getTokens(self, text):
		print "[NLTK] Making text lowercase..."
		lowers = text.lower()
		clean = ' '.join([w for w in lowers.split() if not w in self.lines])
		#remove the punctuation using the character deletion step of translate
		print "[NLTK] Removing punctuation..."
		remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)
		no_punctuation = clean.translate(remove_punctuation_map)
		print "[NLTK] Tokenizing words..."
		#no_short = [w for w in no_punctuation if len(w) > 4 and not w.islower()]
		try:
			tokens = nltk.word_tokenize(no_punctuation)
		except:
			nltk.download("punkt")
			tokens = nltk.word_tokenize(no_punctuation)
		#pprint.pprint("type punt: " + no_punctuation)
		return tokens

	def getStopwords(self):
		import stopwords
		return stopwords.english()

	def stopWordRemoval(self,language = "eng", tokens = ""):
		if len(tokens) == 0:
			tokens = self.getTokens()
		#if "eng" in language:
		print "[NLTK] Removing stopwords..."
		filteredEng = [w for w in tokens if not w in self.getStopwords()]
		try:
			filtered = [w for w in filteredEng if not w in self.lines]
		except:
			filtered = filteredEng
		print "[NLTK] After it removal: %s" % len(filtered)

		#sys.exit(2)
		count = Counter(filtered)
		pprint.pprint("[NLTK] After stopwordremoval: "+str(count.most_common(100)))
		return filtered


	def getTfIdfFile(self, filename = ""):
		"""Reads tfidf dictionary from file."""

		if len(filename) == 0:
			filename = self.output_dir + "/" + self.tfidf_name
		print "[NLTK] Fetching TfIdf values from %s..." % filename
		lines = self.fileop.readFileByLine(fileName = filename)
		words = {}
		if lines is not False:
			for line in lines:
				item = line.split(",")
				word = item[0]
				tfidf = float(item[1])
				words[word] = tfidf		
			print "Read %s words..." % len(lines)
		return words


	def stemTokens(self, tokens, stemmer):
		from nltk.stem.porter import *
		
		stemmed = []
		for item in tokens:
			stemmed.append(stemmer.stem(item))
		return stemmed

	def tokenize(self,text):
		tokens = nltk.word_tokenize(text)
		stems = self.stemTokens(tokens, stemmer)
		return stems

	def calculateTfIdf(self, text):
		"""Calculate tf idf."""
		token_dict = {}
		tokens = self.getTokens(text)
		print "[NLTK][calculateTfIdf] Tokens: %s" % len(tokens)
		filtered = self.stopWordRemoval(tokens = tokens)
		count = Counter(tokens)
		print "[NLTK] Most common tokens: " + str(count.most_common(10))
		#stemmer = PorterStemmer()
		print "[NLTK] Stemming words..."
		stemmed = self.stemTokens(filtered, stemmer)
		count = Counter(stemmed)
		print "[NLTK] Most common stemmed words: "+str(count.most_common(100))
		words={}
		wordsall=[]
		for tup in count.most_common(200):
			word = tup[0]
			occ = tup[1]
			if word not in wordsall:
				words[word]=occ
				
				wordsall.append(word)
		#print len(words)
		
		self.saveAnalysedWords(words,path="most_common_stemmed_words_"+self.getDate()+".csv")
		tfidf = TfidfVectorizer(tokenizer=self.tokenize, stop_words='english')
		self.tfidf = tfidf
		tfs = tfidf.fit_transform(tokens)
		return tfs,tfidf

	def analyseText(self, text, tfs, tfidf = None, save = True):
		if tfidf is None:
			tfidf = self.tfidf
		print "[NLTK] Analysing text...Total words: %s" % len(text)
		response = tfidf.transform([text])
		#print response
		feature_names = tfidf.get_feature_names()
		words = {}
		for col in response.nonzero()[1]:
			words[feature_names[col]] = response[0, col]
		if save is True:
			self.saveAnalysedWords(words)
		return words

	def saveAnalysedWords(self, words, path = ""):
		w = 0
		if len(path) == 0:
			path = self.tfidf_name
		words = sorted(words.items(), key=lambda x:x[1], reverse=True)
		print "[NLTK] Total words: %s" % len(words)
		for tup in words:
			word = tup[0]
			value = tup[1]
			item = word.encode('utf8')+", "+str(value)+"\n"
			created = self.fileop.saveFile(data = item, fileName = path, folder = self.output_dir)
			if created is True:
				w +=1
		print "[NLTK] Saved TfIdf file to %s" % self.output_dir+"/"+path

		return w



		




