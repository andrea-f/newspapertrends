#!/usr/bin/python
# -*- coding: utf-8 -*-
import feedparser
import re
import datetime
import clusters

class CreateMatrix(object):
    """Creates and saves a matrix of words and their occurrences."""	
    def __init__(self, abs_path = "", filename= "newspapers"):
        """"""
        self.abs_path = abs_path        
        self.fn = self.getFullFileName(filename = filename, abs_path = abs_path)

    def getDate(self, separator = "_"):
        today = datetime.date.today()
        dt = str(today.day)+separator+str(today.month)+separator+str(today.year)
        return dt

    def getFullFileName(self, filename = "newspapers", abs_path = ""):
        
        if len(abs_path) == 0:
            abs_path = self.abs_path
        dt = self.getDate()
        if abs_path not in filename:
            fn = abs_path+"/"+filename
        else:
            fn = filename
        if dt not in filename:
            fn = fn + "-" +dt
        if not fn.endswith('.txt'):
            fn = fn +'.txt'
        return fn

    def readfile(self,filename = ""):
        """Reads a matrix file from the fs"""
        if len(filename) == 0:
            filename = self.fn
        (rownames, colnames, data) = clusters.readfile(filename)
        return (rownames, colnames, data)

    def hierarchical_clustering(self, filename = ""):
        """Creates an hierarchical set of clusters."""
        if len(filename) == 0:
            filename = self.fn
        #if not filename.endswith('.jpg'):
        #    fn = filename+".jpg"
        #else:
        #    fn = filename

        try:
            blognames, words, data  = clusters.readfile(filename)
        except:
            fn = self.getFullFileName(filename = filename)
            blognames, words, data  = clusters.readfile(fn)
        clust = clusters.hcluster(data)
        #return clust
        return {
            "titles":blognames,
            "words": words,
            "data": data,
            "clusters": clust
        }

    def draw_dendrogram(self, clust, blognames, filename="", jpeg = "clusters.jpg"):
        """Draws and saves dendrogram on disk."""
        import random
        r=random.randint(0, 1000)
        if len(filename) == 0:
            filename = self.abs_path
        if len(filename) != 0:
            fn = filename+"/"+self.getDate()+"_"+str(r)+"_"+jpeg
        else:
            fn = jpeg
        clusters.drawdendrogram(clust, blognames, fn)
        return fn

    def saveMatrix(self, apcount, testate, abs_path = "", filename = "newspapers",  min = 0.1, max = 10.0):
        """Preformat data and saves matrix on file.

        :param testate:
            Newspaper names, dict.
        """
        wordlist = []
        tt = 0
        tname = []
        if len(abs_path) == 0:
            abs_path == self.abs_path
        for testata,valore in testate.items():
            if testata not in tname:
                tname.append(testata)
                for word, c in valore.items():
                    #print word, c
                    tt+= c
        
        print "Total words: %s" % tt
        for (w, bc) in apcount.items():
            frac = float(bc) / tt
            #print frac, bc
            if frac > min and frac < max:
                print frac,bc, w
                wordlist.append(w)
        # Get a date object
        try:
            fn = self.getFullFileName(filename = filename, abs_path = abs_path)
        except:
            fn = self.fn
        print "Saving to: %s" % fn
        print "Wordlist len: %s" % len(wordlist)
        out = file(fn, 'w')
        out.write(filename)
        for word in wordlist:
            out.write('\t%s' % word.encode('utf8'))
        out.write('\n')
        wordcounts = self.generateWordcounts(testate)
        for (blog, wc) in wordcounts.items():
            print blog
            out.write(blog)
            for word in wordlist:
                if word in wc:
                    out.write('\t%d' % wc[word])
                else:
                    out.write('\t0')
            out.write('\n')
        return wordlist

    def generateWordcounts(self, testate ):
        wordcounts = {}
        for testata,wc in testate.items():

            try:
                wordcounts[testata] = wc
                
            except:
                print 'Failed to parse feed %s' % testata
        return wordcounts
