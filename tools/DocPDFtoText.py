from pdfminer.pdfparser import PDFDocument, PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter, process_pdf
from pdfminer.pdfdevice import PDFDevice, TagExtractor
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.cmapdb import CMapDB
from pdfminer.layout import LAParams
from subprocess import Popen, PIPE
from docx import opendocx, getdocumenttext
from cStringIO import StringIO
from lxml import html
from pyPdf import PdfFileReader
from StringIO import StringIO
import requests
# python manage.py celery worker --loglevel=info7
import codecs
import os,sys, traceback
sys.stdout = codecs.getwriter('UTF-8')(sys.stdout)

class ConvertDocumentToText():
	def __init__(self):
		"""Converts .pdf or .doc to plain text."""
	def scrap_pdf(self, url):
		"""Convert .pdf to text."""
	        rsrcmgr = PDFResourceManager()
		retstr = StringIO()
		codec = 'utf-8'
		laparams = LAParams()
		device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
		fp = file(url, 'rb')
		process_pdf(rsrcmgr, device, fp)
		fp.close()
		device.close()
		str = retstr.getvalue()
		retstr.close()
		return str

	def scrap_pdf_other(self, a):
		text_content = ""
		try:
			print "[DocPDFtoText.scrap_pdf_other] Fetching pdf...."
			pdf = requests.get(a)
		except Exception as e:
			print "[DocPDFtoText.scrap_pdf_other] Error in fetching pdf: %s " % e
		try:
		        pdfFile=PdfFileReader(StringIO(pdf.content))
		        pdfInfos=pdfFile.getDocumentInfo()
		        nb_pages=pdfFile.getNumPages()
		        
		        author=repr(pdfInfos.author)
		        
		        creator=repr(pdfInfos.creator)
		        
		        producer=repr(pdfInfos.producer)
		        
		        subject=repr(pdfInfos.subject)
		        
		        title=repr(pdfInfos.title)
		        
		        #text_content=""
	                for i in range(1,pdfFile.getNumPages(),1):
	                    page=pdfFile.getPage(i)
	                page=pdfFile.getPage(1)
	                for i in range(2,pdfFile.getNumPages(),1):
	                    page.mergePage(pdfFile.getPage(i))
	                text_content=page.extractText()
			try:
				print "[DocPDFtoText.scrap_pdf_other] Nb pages : "+repr(nb_pages)
				print "[DocPDFtoText.scrap_pdf_other] Author : "+author
				print "[DocPDFtoText.scrap_pdf_other] Creator : "+creator
				print "[DocPDFtoText.scrap_pdf_other] Producer : "+producer
				print "[DocPDFtoText.scrap_pdf_other] Subject : "+subject
				print "[DocPDFtoText.scrap_pdf_other] Title : "+title
				print "[DocPDFtoText.scrap_pdf_other] Scraped Text : "+text_content
			except:
				pass
            	except Exception as e:
		        print '[DocPDFtoText.scrap_pdf_other] Unexpected error while reading PDF file : %s error: %s' % (a,e)
		        pass
		return {
			"text":' '.join([author, creator, producer, subject, title, text_content]),
			"title": title
		}			

	def scrap_doc(self, filename, file_path = ""):
		"""Convert .doc to text."""
		if ".docx" in filename:
			document = opendocx(file_path)
			paratextlist = getdocumenttext(document)
			newparatextlist = []
			for paratext in paratextlist:
			    newparatextlist.append(paratext.encode("utf-8"))
			return {
				"text":'\n\n'.join(newparatextlist),
				"title": ""
			}
		elif ".docx" in filename:
			cmd = ['antiword', file_path]
			p = Popen(cmd, stdout=PIPE)
			stdout, stderr = p.communicate()
			return {
				"text":stdout.decode('ascii', 'ignore'),
				"title": ""
			}
		
		elif ".odt" in filename:
			cmd = ['odt2txt', file_path]
			p = Popen(cmd, stdout=PIPE)
			stdout, stderr = p.communicate()
			return {
				"text":stdout.decode('ascii', 'ignore'),
				"title": ""
			}
