# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 06:59:20 2014
 
@author: Sukhbinder Singh
 
Tarfile test
 
"""
import tarfile,sys
 
def untar(fname, ending = "tar.gz"):
    if (fname.endswith(ending)):
        tar = tarfile.open(fname)
        tar.extractall()
        tar.close()
        print "Extracted in Current Directory"
    else:
        print "Not a tar.gz file: '%s '" % sys.argv[0]
 
if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: '%s filename'" % sys.argv[0]
        sys.exit(0)
    untar(sys.argv[1])
