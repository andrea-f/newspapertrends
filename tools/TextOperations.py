import re
import nltk
class TextOperations():
    """Applies useful text transformation and information extraction."""
    

    LANGUAGES = {
        "eng":"en_US",
        "it":"it_IT"
        }

    def __init__(self):
        """"""

    def getSentences(self, text = "Some text. To split into sentences.", splitter = "\\n"):
        """Divide text into sentences.

        :param text:
            Blob of text to split into sentences, string.

        Return list with sentences.
        """
        import nltk.data
        tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
        #text = nltk.corpus.gutenberg.raw('chesterton-thursday.txt')

        sents = tokenizer.tokenize(text)
        clearSents = []
        nonEmptyClearSents = []
        for sent in sents:
            s1 = sent.split(splitter)
            clearSents = clearSents + s1
        for s in clearSents:
            pattern = re.compile(r'\{[^)]*\}', re.DOTALL)
            allCit = re.findall(pattern, text)
            for cit in allCit:
                s.replace(cit,'')
            if len(s) != 0:
                nonEmptyClearSents.append(s)
        return nonEmptyClearSents

    def spellCheck(self, text, language, default = "en_US"):
        """Perform spellcheck on text with pyEnchant, return errors

        :param text:
            Information to analyse, string.
        :param language:
            Vocabulary to use for correcting grammar, string.
        :param default:
            If no language is specified, string.

        """
        import enchant
        if self.LANGUAGES[language]:
            lang = self.LANGUAGES[language]
        else:
            lang = default
        d = enchant.Dict(lang)
        #return errors
        return [word for word in text.split() if d.check(word) is False]

    def mostCommonWordsInPage(self,root = "", minFreq = 0.1, maxFreq = 1.0, page = "This is a massive amount of text"):
        """Extracts most common words in page.

        :param page:
            Set of text to extract most common words from, string.

        :param root:
            Main category/root of page, string.

        :param minFreq:
            Minimum frequency of word to be considered, float.

        :param maxFreq:
            Maximum frequency of word to be considered, float.

        Return list with most common words(of frequency between 10% and 50% and their frequency on the page.
        """
        
        wordCount = {}
        words = self._getWords(page)
        #print "[mostCommonWordsInPage] Total words: " + str(len(words))
        for word in words:
            if (len(word) > 3) and (("US" or "USA" or "UK") not in word):
                wordCount.setdefault(word,0)
                wordCount[word]+= 1
        wordList = []
        for w in sorted(wordCount, key=wordCount.get):
            frac = (float(wordCount[w])/len(words)) * 100
            #print w, wordCount[w], frac
            if frac>minFreq and frac<maxFreq: wordList.append(w)
        if len(root)>0:
            r = root.replace('-', ' ').lower()
            wordList.append(r)
        return wordList

    def _getWords(self, html):
        """Strips all HTML and other tags from page.

        :param html:
            Text to remove html tags from, string.

        Return list with clean words.
        """
        try:
            txt2 = str(html.encode('utf-8'))
        except:
            txt2 = str(html)
        txt = re.compile(r'<[^>]+>').sub('', txt2)
        words = re.compile(r'[^A-Z^a-z]+').split(txt)
        return [word.lower() for word in words if word != '']

    def getWordCount(self, document):
        """Calculates frequency distribution of inserted text."""
        #if len(wordFeatures) == 0:
        words = []
        all = document.split()
        for w in all:
            stop = self.stopWord(w)
            if stop is False:
                words.append(w)
        wordlist = nltk.FreqDist(words)
        #for wordpos in words:
         #   parole.setdefault(wordpos,0)
          #  parole[wordpos] += 1
        #print parole
        #print "-------------------------"
        #print wordlist
        return wordlist

    def stopWord(self,word, minlen = 4):
        stop = True
        stopwords = ["quell", "quest"]
        rep = [",",":","."]
        for r in rep:
            word = word.replace(r,'')
        #if value > 4 and len(word)> 3 and not word.isupper() and (len(word)>5 or not word.islower()) and word.isalpha():

        if word.isalpha():
            if len(word)>5 or not word.islower():
                if len(word)> minlen:
                    for s in stopwords:
                        if s not in word:
                            stop = False
        return stop