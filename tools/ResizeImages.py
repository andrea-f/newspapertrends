import PIL
from PIL import Image
import os, sys
from time import gmtime, strftime
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ )))
#sys.path.append(lib)
input_dir = sys.argv[1]
if input_dir.endswith('/'): input_dir = input_dir[:-1]
basewidth = 1200
baseheight = 900
files_in_dir = os.listdir(input_dir)
date_now = strftime("%Y-%m-%d %H:%M:%S", gmtime())
#'2009-01-05 22:14:39'
save_dir = input_dir+'/1200x900_'+date_now.split()[0]
res = 0
for f in files_in_dir:
	if f.lower().endswith('.jpg'): 
		img = Image.open(input_dir+"/"+f)
		wpercent = (basewidth / float(img.size[0]))
		hsize = int((float(img.size[1]) * float(wpercent)))
		img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
		try:
			os.makedirs(save_dir)
			print("Created %s" % save_dir)
		except OSError, exc:
			pass
		img.save(save_dir+ "/" + f)
		res += 1
		print("Resized to %sx%s %s images in %s." % (basewidth,baseheight,res,save_dir) )