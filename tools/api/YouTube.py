class SearchVideos():
    """This class searches videos based on an event."""


    def __init__(self, event = None):
        """Does nothing for now.
        
        :param event:
            From :class:`Event`, object.            
        """
        self.event = event

    def _setUpYTApi(self):
        """Sets up YouTube API client.

        Return YouTube API object.
        """
        import gdata.youtube
        import gdata.youtube.service
        ytService = gdata.youtube.service.YouTubeService()
        ytService.ssl = False
        return ytService

    def _setUpYTQuery(self, query = "", startIndex = 1, maxResults = 50):
        """Sets up YouTube Query object.

        :param query:
            Which query to run on youtube, string.

        Return YouTube Query object.
        """
        import gdata.youtube
        import gdata.youtube.service
        #query.orderby = 'viewCount'
        queryyt = gdata.youtube.service.YouTubeVideoQuery()
        queryyt.racy = 'include'
        queryyt.orderby = 'relevance'
        #queryyt.safeSearch = 'none'
        queryyt.start_index = startIndex
        queryyt.max_results=maxResults
        queryyt.vq = query
        return queryyt

    def runYouTubeQuery(self, query, pages = 0, maxResults = 50):
        """Retrieves video feed based on query build on YouTube service.

        :param query:
            YouTube Query class item, object.

        Return list with video feed.
        """
        ytService = self._setUpYTApi()
        page = 0
        index = 1
        f = []
        while page <= pages:
            if pages==0:
                queryObject = self._setUpYTQuery(query = query)
            else:
                queryObject = self._setUpYTQuery(query = query, startIndex = index)
                index += maxResults                
            try:
                feed = ytService.YouTubeQuery(queryObject)
                if pages>0:
                    if len(feed.entry)>0:
                        f += feed.entry
                #print feed
                print "[runYouTubeQuery] " + query + " RESULTS: " + str(len(feed.entry))
            except Exception as e:
                print "[runYouTubeQuery] Error for " + query + " reason: " + str(e)
                feed = None
            page += 1
        try:
            if pages > 0:
                feed.entry = f
        except:
            pass
        return feed

    def getVideo(self, id = "2NxVMcf6TFc"):
        """Get YouTube video by id.
        
        :param id:
           11-chars matching specific YouTube video ID, string.
 
        Return :class:`gdata.youtube` object with video entry.
        """
        ytService = self._setUpYTApi()
        entry = ytService.GetYouTubeVideoEntry(video_id = id)
        return entry


    def createVideoObject(self, entry = {},extraWords = [], event = {}, items_name = [], q = "Query which got the video", date = 0, eventName = "",root = "", category = ""):
        """Instantiate :class:`Video` objects.

        :param entry:
            YouTube video information, object.

        :param event:
            It is :class:`Event` instance, object.

        :param q:
            Query which got the video, string.

        Return :class:`Video` object.
        """
        import Video
        video = Video.Video(
            title = entry.media.title.text,
            category = entry.media.category[0].label,
            query = q,
            url = entry.GetSwfUrl(),
            duration = entry.media.duration.seconds,
            eventName = eventName,
            image = entry.media.thumbnail[1].url,
            root = root,
            categoryOfEvent = category,
            uploadDate = entry.published.text,
            uploader = entry.author[0].name.text,
            helperWords = extraWords,
            items = items_name
        )
        try:
            video.viewcount = entry.statistics.view_count
        except:
            pass
        try:
            video.description = entry.media.description.text
        except:
            pass
        try:
            video.keywords= entry.media.keywords.text
        except:
            pass
        try:
            if date == 0:
                video.date = queries[0].split()[0]
            else:
                video.date = date
        except:
            video.date = date
        return video