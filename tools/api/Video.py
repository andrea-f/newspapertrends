class Video(object):
    """This class creates an object containing a url query score and items."""

    def __init__(self,
        lev = None,
        eventHash = None,
        uploader = None,
        url = None,
        query = None,
        normalized = None,
        score = None,
        items = [],
        item_names = [],
        date = None,
        category = None,
        title = None,
        duration = None,
        image = None,
        eventName = None,
        description = None,
        viewcount = None,
        keywords = None,
        categoryOfEvent = None,
        uploadDate = None,
        helperWords = None,
        root = None,
        **videohash):
        self.eventHash = eventHash
        self.url = url
        self.query = query
        self.score = score
        self.items = items
        self.date = date
        self.category = category
        self.description = description
        self.eventName = eventName
        self.title = title
        self.duration = duration
        self.image = image
        self.viewcount = viewcount
        self.keywords = keywords
        self.categoryOfEvent = categoryOfEvent
        self.uploadDate = uploadDate
        self.helperWords = helperWords
        self.root = root
        self.uploader = uploader
        self.normalized = normalized        
        self.lev = lev
        self.item_names = item_names


