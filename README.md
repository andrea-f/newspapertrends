hownewspaperswrite
==================

This tools allows aggregated spell check monitoring for Newspaper websites.

1.  First make sure you have Docker and docker-compose installed
2.  Build the docker image: 
    ```
    docker build -t andreaf/news_env_v2 .
    ```
3.  Run:
    ```
    docker-compose -f docker_compose.yml up -d 
    ```
4.  Set up Django site:
    ```
    docker exec -i -t CONTAINER_ID /bin/bash
    ```
5.  Navigate to:
    ```
    cd /code/django/hownewspaperswrite
    ```
6.  Run:
    ```
    python manage.py syncdb
    ```
7.  Populate the database:
    ```
    cd /code/bin/test/
    ```
    then
    ```
    sh process_news.sh
    ```
8.  Run 
    ```
    python manage.py runserver 
    ```
9.  Navigate to and create a summary:
    ```
    http://IP_ADDRESS:8000/statistiche/
    ```

10.  Navigate to:
    ```
    http://IP_ADDRESS:8000/summaries/
    ```